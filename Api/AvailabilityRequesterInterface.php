<?php
declare(strict_types=1);

namespace Beside\Sourcing\Api;

use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\Data\OrderInterface;

/**
 * Interface AvailabilityRequesterInterface
 *
 * @package Beside\Sourcing\Api
 */
interface AvailabilityRequesterInterface
{
    /**
     * XML path to max count of order limit setting
     */
    public const XML_PATH_TRANSFER_ORDER_LIMIT = 'beside_sourcing/general/transfer_order_limit';

    /**
     * XML path to max count of order limit exception message
     */
    public const XML_PATH_TRANSFER_ORDER_LIMIT_EXCEPTION = 'beside_sourcing/general/transfer_order_limit_exception';
}
