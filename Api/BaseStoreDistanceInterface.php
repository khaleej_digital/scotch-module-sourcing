<?php
declare(strict_types=1);

namespace Beside\Sourcing\Api;

use Beside\Sourcing\Api\Data\StoreDistanceInterface;
use Magento\Framework\Exception\LocalizedException;
use Redbox\Shopfinder\Api\Data\ShopInterface;

/**
 * Interface StoreDistanceCheckerInterface
 *
 * @package Beside\Sourcing\Api
 */
interface BaseStoreDistanceInterface
{
    /**
     * These Shopfinder fields are required for distance calculation
     */
    public const DISTANCE_REQUIRED_FIELDS = [
        ShopInterface::IDENTIFIER,
        ShopInterface::LATITUDE,
        ShopInterface::LONGITUDE,
        ShopInterface::STORES
    ];

    /** @var string Path to config for cross store shop search  */
    public const XML_PATH_CROSS_STORES_SEARCH = 'beside_sourcing/shop_finding/enable_cross_store_search';

    /**
     * Get distance from DB or calculate it
     *
     * @param string $storeIdFrom
     * @param string $storeIdTo
     *
     * @return int|null
     * @throws LocalizedException
     */
    public function getDistance(string $storeIdFrom, string $storeIdTo): ?int;

    /**
     * Save distance to DB table
     *
     * @param string $storeIdFrom
     * @param string $storeIdTo
     * @param int $distance
     *
     * @return StoreDistanceInterface
     * @throws LocalizedException
     */
    public function saveDistance(string $storeIdFrom, string $storeIdTo, int $distance): StoreDistanceInterface;

    /**
     * Find a store from the array that is closer to the "main" store
     *
     * @param string $mainStoreId
     * @param array $stores
     *
     * @return string
     * @throws LocalizedException
     */
    public function getNearestStore(string $mainStoreId, array $stores): string;

    /**
     * Add or update distance table based on provided store coordinate arrays
     *
     * @param array $storeDataFrom
     * @param array $storeDataTo
     *
     * @throws LocalizedException
     */
    public function addDistance(array $storeDataFrom, array $storeDataTo): void;
}
