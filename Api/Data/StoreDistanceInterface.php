<?php
declare(strict_types=1);

namespace Beside\Sourcing\Api\Data;

/**
 * Interface StoreDistanceInterface
 *
 * @package Beside\Sourcing\Api\Data
 */
interface StoreDistanceInterface
{
    /**
     * Cache tag
     */
    public const CACHE_TAG = 'beside_store_distance';

    /**
     * Table name
     */
    public const STORE_DISTANCE_TABLE = 'beside_store_distance';

    /**
     * Table columns
     */
    public const ID = 'id';
    public const FROM = 'store_from';
    public const TO = 'store_to';
    public const DISTANCE = 'distance';

    /**
     * Get entity ID
     *
     * @return mixed
     */
    public function getId();

    /**
     * Get store identifier "from"
     *
     * @return string
     */
    public function getStoreIdFrom(): string;

    /**
     * Set store identifier "from"
     *
     * @param string $storeId
     *
     * @return $this
     */
    public function setStoreIdFrom(string $storeId): StoreDistanceInterface;

    /**
     * Get store identifier "to"
     *
     * @return string
     */
    public function getStoreIdTo(): string;

    /**
     * Set store identifier "to"
     *
     * @param string $storeId
     *
     * @return $this
     */
    public function setStoreIdTo(string $storeId): StoreDistanceInterface;

    /**
     * Get distance
     *
     * @return int
     */
    public function getDistance(): int;


    /**
     * Set distance
     *
     * @param mixed $distance
     *
     * @return $this
     */
    public function setDistance($distance): StoreDistanceInterface;
}
