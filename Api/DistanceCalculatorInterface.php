<?php
declare(strict_types=1);

namespace Beside\Sourcing\Api;

/**
 * Interface DistanceCalculatorInterface
 *
 * @package Beside\Sourcing\Api
 */
interface DistanceCalculatorInterface
{
    /**
     * Calculate distance between two locations
     *
     * @param array $coordinatesFrom
     * @param array $coordinatesTo
     *
     * @return int
     */
    public function calculateDistance(array $coordinatesFrom, array $coordinatesTo): int;
}
