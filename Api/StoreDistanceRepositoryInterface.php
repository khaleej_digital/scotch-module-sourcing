<?php
declare(strict_types=1);

namespace Beside\Sourcing\Api;

use Beside\Sourcing\Api\Data\StoreDistanceInterface;
use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Interface StoreDistanceRepositoryInterface
 *
 * @package Beside\Sourcing\Api
 */
interface StoreDistanceRepositoryInterface
{
    /**
     * Save StoreDistance
     *
     * @param StoreDistanceInterface $storeDistance
     * @return StoreDistanceInterface
     * @throws LocalizedException
     */
    public function save(
        StoreDistanceInterface $storeDistance
    );

    /**
     * Retrieve StoreDistance
     *
     * @param string|int $id
     *
     * @return StoreDistanceInterface
     * @throws LocalizedException
     */
    public function get($id);

    /**
     * Retrieve StoreDistance matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete StoreDistance
     *
     * @param StoreDistanceInterface $storeDistance
     *
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(StoreDistanceInterface $storeDistance);

    /**
     * Delete StoreDistance by ID
     *
     * @param string|int $Id
     *
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($Id);
}
