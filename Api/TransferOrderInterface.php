<?php
declare(strict_types=1);

namespace Beside\Sourcing\Api;

/**
 * Interface TransferOrderInterface
 *
 * @package Beside\Sourcing\Api
 */
interface TransferOrderInterface
{
    /**
     * Add Transfer Order API message to the queue
     *
     * @param array $transObject
     * @param mixed $storeId
     */
    public function addTransferToQueue(array $transObject, $storeId): void;

    /**
     * @param $orderId
     * @param array $transfers
     * @param string $mainStore
     * @param string|null $salesChannelId
     * @return array
     */
    public function prepareTransferOrderMessage($orderId, array $transfers, string $mainStore, ?string $salesChannelId = null);
}
