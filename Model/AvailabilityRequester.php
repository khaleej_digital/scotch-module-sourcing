<?php
declare(strict_types=1);

namespace Beside\Sourcing\Model;

use Beside\Erp\Api\StockCheckApiInterface;
use Beside\Sourcing\Api\AvailabilityRequesterInterface;
use Beside\Sourcing\Api\BaseStoreDistanceInterface;
use Beside\Sourcing\Api\DistanceCalculatorInterface;
use Beside\Sourcing\Api\TransferOrderInterface;
use Exception;
use InvalidArgumentException;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Store\Model\Store;
use Psr\Log\LoggerInterface;

/**
 * Class AvailabilityRequester
 *
 * @package Beside\Sourcing\Model
 */
class AvailabilityRequester implements AvailabilityRequesterInterface
{
    /**
     * @var BaseStoreDistanceInterface
     */
    private BaseStoreDistanceInterface $baseStoreDistance;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var TransferOrderInterface
     */
    private TransferOrderInterface $transferOrder;

    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;

    /**
     * @var DistanceCalculatorInterface
     */
    private DistanceCalculatorInterface $distanceCalculator;

    /**
     * @var StockCheckApiInterface
     */
    private StockCheckApiInterface $stockCheckApi;

    /**
     * @var Json
     */
    private Json $json;

    /**
     * @var array
     */
    private $currentItemsArray = [];

    /**
     * @var float|null
     */
    private $customerLongitude;

    /**
     * @var float|null
     */
    private $customerLatitude;

    /**
     * @var array
     */
    private $usedSources = [];

    /**
     * AvailabilityRequester constructor.
     *
     * @param BaseStoreDistanceInterface $baseStoreDistance
     * @param TransferOrderInterface $transferOrder
     * @param ScopeConfigInterface $scopeConfig
     * @param DistanceCalculatorInterface $distanceCalculator
     * @param StockCheckApiInterface $stockCheckApi
     * @param Json $json
     * @param LoggerInterface $logger
     */
    public function __construct(
        BaseStoreDistanceInterface $baseStoreDistance,
        TransferOrderInterface $transferOrder,
        ScopeConfigInterface $scopeConfig,
        DistanceCalculatorInterface $distanceCalculator,
        StockCheckApiInterface $stockCheckApi,
        Json $json,
        LoggerInterface $logger
    ) {
        $this->baseStoreDistance = $baseStoreDistance;
        $this->transferOrder = $transferOrder;
        $this->scopeConfig = $scopeConfig;
        $this->distanceCalculator = $distanceCalculator;
        $this->stockCheckApi = $stockCheckApi;
        $this->json = $json;
        $this->logger = $logger;
    }

    /**
     * @param array $transferOrderCandidates
     * @param int $orderId
     * @param string $predefinedSourceId
     * @return array
     * @throws ItemsAvailabilityException
     */
    private function prepareTransferOrder(array $transferOrderCandidates, $orderId, $predefinedSourceId)
    {
        $transferOrders = [];
        $this->excludeUsedSource($predefinedSourceId);

        while (!empty($transferOrderCandidates) || !empty($this->currentItemsArray)) {
            $recentSourceId = $this->getSourceForTransferOrder($predefinedSourceId);

            foreach ($this->currentItemsArray as $sku => $stockData) {
                if (isset($transferOrderCandidates[$sku])) {
                    $missingQty = $transferOrderCandidates[$sku];
                    $qtyForTO = 0;

                    foreach ($stockData['items'] as $item) {
                        if ($item['source_id'] == $recentSourceId && (int)$item['quantity']) {
                            if ($item['quantity'] >= $missingQty) {
                                $qtyForTO = (int) $missingQty;
                                unset($transferOrderCandidates[$sku]);
                            } else {
                                $qtyForTO = (int) $item['quantity'];
                                $transferOrderCandidates[$sku] = $missingQty - $qtyForTO;
                            }
                            break;
                        }
                    }
                } else {
                    unset($this->currentItemsArray[$sku]);
                    continue;
                }

                if ($qtyForTO) {
                    $transferOrders[$recentSourceId][] = [$sku => $qtyForTO];
                }
            }

            $this->excludeUsedSource($recentSourceId);
        }

        $this->checkTransferOrderLimit($transferOrders);

        return $this->transferOrder->prepareTransferOrderMessage($orderId, $transferOrders, $predefinedSourceId);
    }

    /**
     * @param string $sourceId
     * @return string|null
     * @throws LocalizedException
     */
    private function getSourceForTransferOrder($sourceId)
    {
        $prepearedItems = $this->getItemsQty($this->currentItemsArray);
        $result = $this->getStoreWithMaxItems($prepearedItems, $sourceId);

        return $result;
    }

    /**
     * @param string $sourceId
     */
    private function excludeUsedSource($sourceId)
    {
        foreach ($this->currentItemsArray  as $sku => $stocksData) {
            foreach ($stocksData['items'] as $key => $data) {
                if ($data['source_id'] == $sourceId) {
                    unset($this->currentItemsArray [$sku]['items'][$key]);
                    $this->usedSources[] = $sourceId;
                }
            }
        }
    }

    /**
     * Check transfer order limit
     *
     * @param array $transfers
     *
     * @throws ItemsAvailabilityException
     */
    private function checkTransferOrderLimit(array $transfers)
    {
        $count = 0;
        $limit = $this->getConfigValue(self::XML_PATH_TRANSFER_ORDER_LIMIT);
        foreach ($transfers as $store => $transfer) {
            $count += count($transfer);
            if ($count > $limit) {
                $message = $this->getConfigValue(self::XML_PATH_TRANSFER_ORDER_LIMIT_EXCEPTION);
                throw new ItemsAvailabilityException(__($message));
            }
        }
    }

    /**
     * Read config value by xml path
     *
     * @param $path
     * @param string $scope
     *
     * @return string|null
     */
    private function getConfigValue($path, $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT): ?string
    {
        return $this->scopeConfig->getValue($path, $scope);
    }

    /**
     * Get item stock, send request to Stock Check API
     *
     * @param OrderInterface $order
     *
     * @return array
     * @throws ItemsAvailabilityException
     * @throws Exception
     */
    public function getItemsStock(OrderInterface $order): array
    {
        $itemsArray = [];

        foreach ($order->getAllVisibleItems() as $item) {
            $qty = $item->getQtyOrdered();
            $sku = $item->getSku();
            $message = $this->stockCheckApi->prepareData($sku);
            $stockCheck = $this->stockCheckApi->sendRequest(
                $message,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
                $order->getStoreId()
            );
            try {
                $stock = $this->json->unserialize($stockCheck['response']['value'] ?? []);
            } catch (InvalidArgumentException $e) {
                $stock = $stockCheck['response']['value'] ?? [];
            }
            if (!isset($stock['items'])) {
                $errorMessage = 'Could not parse Stock Check response';
                if (is_string($stock)) {
                    $errorMessage = $stock;
                }
                throw new ItemsAvailabilityException(__($errorMessage));
            }
            $itemsArray[$sku] = $stock;
            $itemsArray[$sku]['requested_qty'] = $qty;
        }

        $this->currentItemsArray = $itemsArray;

        $this->checkStoresTotalQty();

        return $itemsArray;
    }

    /**
     * @param float $lat
     * @param float $long
     * @return array
     */
    private function setCustomerCoordinates($lat, $long)
    {
        $this->customerLatitude = $lat;
        $this->customerLongitude = $long;

        return $this->getCustomersCoordinates();
    }

    /**
     * Getting customer coordinates from order
     *
     * @return array
     */
    private function getCustomersCoordinates()
    {
        return ['latitude' => $this->customerLatitude, 'longitude' => $this->customerLongitude];
    }

    /**
     * @return array
     */
    public function cleanCustomersCoordinates()
    {
        $this->customerLatitude = null;
        $this->customerLongitude  = null;

        return $this->getCustomersCoordinates();
    }

    /**
     * Find store with max requested items quantity
     *
     * @param array $array
     * @param string $mainStoreId
     *
     * @return null|string
     * @throws LocalizedException
     */
    private function getStoreWithMaxItems(array $array, string $mainStoreId = ''): ?string
    {
        $max = 0;
        $stores = [];
        $maxStoreId = null;
        $customerCoordinates = [];
        if (empty($mainStoreId)) {
            $customerCoordinates = $this->getCustomersCoordinates();
        }
        foreach ($array as $store => $storeStock) {
            if ($storeStock['total'] > $max) {
                $stores = [];
                $max = $storeStock['total'];
                $maxStoreId = $store;
                $stores[$maxStoreId] = $max;
            } elseif ($storeStock['total'] == $max) {
                if (empty($stores)) {
                    $maxStoreId = $store;
                    $stores[$maxStoreId] = $array[$maxStoreId]['total'];
                    continue;
                }
                $nextMaxStore = array_search($max, $stores);
                if ($nextMaxStore && $mainStoreId) {
                    $nearestStore = $this->baseStoreDistance->getNearestStore(
                        $mainStoreId,
                        [$store, $nextMaxStore]
                    );
                    $max = $array[$nearestStore]['total'];
                    $maxStoreId = $nearestStore;
                } elseif ($nextMaxStore && $customerCoordinates) {
                    $distanceToCustomer1 = $this->getDistanceToCustomer($customerCoordinates, $store);
                    $distanceToCustomer2 = $this->getDistanceToCustomer($customerCoordinates, $nextMaxStore);
                    $nearestStore = $distanceToCustomer1 < $distanceToCustomer2 ? $store : $nextMaxStore;
                    $max = $array[$nearestStore]['total'];
                    $maxStoreId = $nearestStore;

                } else {
                    $maxStoreId = $store;
                    $stores[$maxStoreId] = $array[$maxStoreId]['total'];
                    unset($array[$store]);
                    continue;
                }
                $stores = [];
                $stores[$maxStoreId] = $array[$maxStoreId]['total'];
            }
        }

        return $maxStoreId;
    }

    /**
     * Get distance from customer to store by customers coordinates and store code
     *
     * @param array $customerCoordinates
     * @param string $storeId
     *
     * @return int
     */
    private function getDistanceToCustomer(array $customerCoordinates, string $storeId)
    {
        $storeCoordinates = $this->baseStoreDistance->getStoreCoordinates($storeId);
        $distance = $this->distanceCalculator->calculateDistance($customerCoordinates, $storeCoordinates);

        return $distance;
    }

    /**
     * Build items stock array from Stock Check API response
     * Order array by store and calculate delta for each SKU (difference between requested and available qty)
     * E.g. uae_ar => [test_sku_1 => 4, test_sku_2 => 2, total => 6, delta => [...]]
     *
     * @param array $items
     *
     * @return array
     */
    public function getItemsQty(array $items): array
    {
        $totalRequestedQty = 0;
        $array = [];
        foreach ($items as $sku => $data) {
            $requestedQty = $data['requested_qty'];
            $totalRequestedQty = $totalRequestedQty + $requestedQty;
            foreach ($data['items'] as $item) {
                $qty = $item['quantity'];
                $availableQty = ($qty >= $requestedQty) ? $requestedQty : $qty;
                $array[$item['source_id']][$sku] = (int) $availableQty;
                $total = $array[$item['source_id']]['total'] ?? 0;
                $array[$item['source_id']]['total'] = (float) $total + $availableQty;
                $array[$item['source_id']]['delta'][$sku] = (int) $availableQty - $requestedQty;
                if ((int)$array[$item['source_id']]['delta'][$sku] == 0) {
                    unset($array[$item['source_id']]['delta'][$sku]);
                }
            }
        }

        return $array;
    }

    /**
     * @param OrderInterface $order
     * @param array $itemsArray
     * @return string|null
     * @throws LocalizedException
     */
    public function resolveConsolidatingStore(OrderInterface $order)
    {
        $this->setCustomerCoordinates($order->getLatitude(), $order->getLongitude());
        $prepearedItems = $this->getItemsQty($this->currentItemsArray);
        $consolidatingStore = $this->getStoreWithMaxItems($prepearedItems);
        $this->cleanCustomersCoordinates();

        return $consolidatingStore;
    }

    /**
     * @param OrderInterface $order
     * @return array
     * @throws ItemsAvailabilityException
     * @throws LocalizedException
     */
    public function getTransferOrderMessages(OrderInterface $order)
    {
        $transferOrderCandidates = [];
        $consolidatingStoreId = $order->getBillingAddress()->getPickupLocationId();
        $orderId = $order->getEntityId();

        foreach ($this->currentItemsArray as $sku => $data) {
            foreach ($data['items'] as $stockItem) {
                if ($stockItem['source_id'] == $consolidatingStoreId ) {
                    $qtyInSource = $stockItem['quantity'];

                    if ($data['requested_qty'] > $qtyInSource) {
                        /** @var \Magento\Sales\Api\Data\OrderItemInterface $item */
                        foreach ($order->getAllVisibleItems() as $item) {
                            if ($item->getSku() == $stockItem['sku']) {
                                $missingQty = $qtyInSource <= 0
                                    ? $data['requested_qty']
                                    : $data['requested_qty'] - $qtyInSource;

                                $transferOrderCandidates[$stockItem['sku']] = $missingQty;
                            }
                        }
                    }
                }
            }
        }

        if (empty($transferOrderCandidates)) {
            return $transferOrderCandidates;
        }

        return $this->prepareTransferOrder($transferOrderCandidates, $orderId, $consolidatingStoreId);
    }

    /**
     * Unset $currentItemsArray
     */
    public function cleanCurrentItemsArray()
    {
        $this->currentItemsArray = [];
    }

    /**
     * Check if there are enough items in all stores to complete the order
     *
     * @return bool
     * @throws ItemsAvailabilityException
     */
    private function checkStoresTotalQty(): bool
    {
        $result = true;
        foreach ($this->currentItemsArray as $sku => $stockData) {
            $sum = 0;
            foreach ($stockData['items'] as $item) {
                $sum += $item['quantity'];
            }
            if ($sum < $stockData['requested_qty']) {
                throw new ItemsAvailabilityException(__('Not enough items\' quantity in stores'));
            }
        }

        return $result;
    }
}
