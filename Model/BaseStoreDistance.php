<?php
declare(strict_types=1);

namespace Beside\Sourcing\Model;

use Beside\Sourcing\Api\BaseStoreDistanceInterface;
use Beside\Sourcing\Api\Data\StoreDistanceInterface;
use Beside\Sourcing\Api\DistanceCalculatorInterface;
use Beside\Sourcing\Api\StoreDistanceRepositoryInterface;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Beside\Sourcing\Model\StoreDistanceFactory;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;
use Redbox\Shopfinder\Api\Data\ShopInterface;
use Redbox\Shopfinder\Api\ShopRepositoryInterface;

/**
 * Class BaseStoreDistance
 *
 * @package Beside\Sourcing\Model
 */
class BaseStoreDistance implements BaseStoreDistanceInterface
{
    /**
     * @var StoreDistanceRepositoryInterface
     */
    private StoreDistanceRepositoryInterface $storeDistanceRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private FilterBuilder $filterBuilder;

    /**
     * @var FilterGroupBuilder
     */
    private FilterGroupBuilder $filterGroupBuilder;

    /**
     * @var StoreDistanceFactory
     */
    private StoreDistanceFactory $storeDistanceFactory;

    /**
     * @var DistanceCalculatorInterface
     */
    private DistanceCalculatorInterface $distanceCalculator;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var ShopRepositoryInterface
     */
    private ShopRepositoryInterface $shopRepository;

    /**
     * DistanceCalculator constructor.
     *
     * @param StoreDistanceRepositoryInterface $storeDistanceRepository
     * @param FilterBuilder $filterBuilder
     * @param FilterGroupBuilder $filterGroupBuilder
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param StoreDistanceFactory $storeDistanceFactory
     * @param DistanceCalculatorInterface $distanceCalculator
     * @param ShopRepositoryInterface $shopRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        StoreDistanceRepositoryInterface $storeDistanceRepository,
        FilterBuilder $filterBuilder,
        FilterGroupBuilder $filterGroupBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        StoreDistanceFactory $storeDistanceFactory,
        DistanceCalculatorInterface $distanceCalculator,
        ShopRepositoryInterface $shopRepository,
        LoggerInterface $logger
    ) {
        $this->storeDistanceRepository = $storeDistanceRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
        $this->storeDistanceFactory = $storeDistanceFactory;
        $this->distanceCalculator = $distanceCalculator;
        $this->logger = $logger;
        $this->shopRepository = $shopRepository;
    }

    /**
     * Get distance from the DB, searching by store identifier
     * Search in DB table saved distance from store A to store B OR from store B to store A
     *
     * @param string $storeIdFrom
     * @param string $storeIdTo
     *
     * @return StoreDistanceInterface|null
     */
    private function getSavedDistance(string $storeIdFrom, string $storeIdTo):?StoreDistanceInterface
    {
        $distance = null;
        $fromFilter = $this->filterBuilder
            ->setField(StoreDistanceInterface::FROM)
            ->setValue($storeIdFrom)
            ->setConditionType('eq')
            ->create();
        $toFilter = $this->filterBuilder
            ->setField(StoreDistanceInterface::TO)
            ->setValue($storeIdFrom)
            ->setConditionType('eq')
            ->create();

        $filterGroups[] = $this->filterGroupBuilder->setFilters([$fromFilter, $toFilter])->create();

        $reverseDirectionTo = $this->filterBuilder
            ->setField(StoreDistanceInterface::TO)
            ->setValue($storeIdTo)
            ->setConditionType('eq')
            ->create();

        $reverseDirectionFrom = $this->filterBuilder
            ->setField(StoreDistanceInterface::FROM)
            ->setValue($storeIdTo)
            ->setConditionType('eq')
            ->create();
        $filterGroups[] = $this->filterGroupBuilder->setFilters([$reverseDirectionTo, $reverseDirectionFrom])->create();

        $searchCriteria = $this->searchCriteriaBuilder->setFilterGroups($filterGroups);

        $searchCriteria = $searchCriteria->create();
        $searchCriteria->setPageSize(1)
            ->setCurrentPage(1);
        $distances = $this->storeDistanceRepository->getList($searchCriteria);
        $distances = $distances->getItems();

        return array_shift($distances);
    }

    /**
     * Get distance from DB or calculate it
     *
     * @param string $storeIdFrom
     * @param string $storeIdTo
     *
     * @return int|null
     * @throws LocalizedException
     */
    public function getDistance(string $storeIdFrom, string $storeIdTo): ?int
    {
        $distanceObject = $this->getSavedDistance($storeIdFrom, $storeIdTo);
        if (!$distanceObject) {
            $coordinatesFrom = $this->getStoreCoordinates($storeIdFrom);
            $coordinatesTo = $this->getStoreCoordinates($storeIdTo);
            $distance = $this->distanceCalculator->calculateDistance($coordinatesFrom, $coordinatesTo);
            $distanceObject = $this->saveDistance($storeIdFrom, $storeIdTo, $distance);
        }
        $distance = $distanceObject->getDistance();

        return $distance;
    }

    /**
     * Get store Latitude and Longitude point in [deg decimal] as array
     *
     * @param string $storeId
     *
     * @return array
     * @throws LocalizedException
     */
    public function getStoreCoordinates(string $storeId): array
    {
        $shop = $this->shopRepository->getById($storeId);

        return ['latitude' => $shop->getLatitude(), 'longitude' => $shop->getLongitude()];
    }

    /**
     * Add or update distance table based on provided store coordinate arrays
     *
     * @param array $storeDataFrom
     * @param array $storeDataTo
     *
     * @throws LocalizedException
     */
    public function addDistance(array $storeDataFrom, array $storeDataTo): void
    {
        $storeIdFrom = $storeDataFrom[ShopInterface::IDENTIFIER];
        $storeIdTo = $storeDataTo[ShopInterface::IDENTIFIER];
        $distance = $this->distanceCalculator->calculateDistance($storeDataFrom, $storeDataTo);
        $this->saveDistance($storeIdFrom, $storeIdTo, $distance);
    }

    /**
     * Save distance to DB table
     *
     * @param string $storeIdFrom
     * @param string $storeIdTo
     * @param int $distance
     *
     * @return StoreDistanceInterface
     * @throws LocalizedException
     */
    public function saveDistance(string $storeIdFrom, string $storeIdTo, int $distance): StoreDistanceInterface
    {
        $storeDistanceObject = $this->getSavedDistance($storeIdFrom, $storeIdTo);
        if (!$storeDistanceObject) {
            /** @var StoreDistanceInterface $storeDistanceObject */
            $storeDistanceObject = $this->storeDistanceFactory->create();
        }
        $storeDistanceObject->setStoreIdFrom($storeIdFrom);
        $storeDistanceObject->setStoreIdTo($storeIdTo);
        $storeDistanceObject->setDistance($distance);

        return $this->storeDistanceRepository->save($storeDistanceObject);
    }

    /**
     * Find a store from the array that is closer to the "main" store
     *
     * @param string $mainStoreId
     * @param array $stores
     *
     * @return string
     * @throws LocalizedException
     */
    public function getNearestStore(string $mainStoreId, array $stores): string
    {
        $minDistance = 0;
        $result = '';
        $i = 0;
        foreach ($stores as $storeId) {
            $distance = $this->getDistance($mainStoreId, $storeId);
            if ($i == 0) {
                $result = $storeId;
                $minDistance = $distance;
            } elseif ($distance < $minDistance) {
                $minDistance = $distance;
                $result = $storeId;

            }
            $i++;
        }

        return $result;
    }
}
