<?php
declare(strict_types=1);

namespace Beside\Sourcing\Model;

use Beside\Sourcing\Api\DistanceCalculatorInterface;

/**
 * Class DistanceCalculator
 *
 * @package Beside\Sourcing\Model
 */
class DistanceCalculator implements DistanceCalculatorInterface
{
    /**
     * Earth radius im meters
     */
    private const EARTH_RADIUS = 6371000;

    /**
     * Calculate distance between two stores
     *
     * @param array $coordinatesFrom
     * @param array $coordinatesTo
     *
     * @return int
     */
    public function calculateDistance(array $coordinatesFrom, array $coordinatesTo): int
    {
        return $this->haversineCircleDistance($coordinatesFrom, $coordinatesTo);
    }

    /**
     * Calculates the great-circle distance between two points with the Haversine formula
     *
     * @param array $from
     * @param array $to
     *
     * @return int Distance between points in meters
     */
    private function haversineCircleDistance(array $from, array $to): int
    {
        // convert from degrees to radians
        $latFrom = deg2rad((float) $from['latitude']);
        $lonFrom = deg2rad((float) $from['longitude']);
        $latTo = deg2rad((float) $to['latitude']);
        $lonTo = deg2rad((float) $to['longitude']);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

        $result = $angle * self::EARTH_RADIUS;

        return (int) round($result);
    }
}
