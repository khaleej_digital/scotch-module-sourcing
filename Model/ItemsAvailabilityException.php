<?php
declare(strict_types=1);

namespace Beside\Sourcing\Model;

/**
 * Class CouldNotGetTransfersException
 *
 * @package Beside\Sourcing\Model
 */
class ItemsAvailabilityException extends \Magento\Framework\Exception\LocalizedException
{
}
