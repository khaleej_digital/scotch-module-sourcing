<?php
declare(strict_types=1);

namespace Beside\Sourcing\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class StoreDistance
 *
 * @package Beside\Sourcing\Model\ResourceModel
 */
class StoreDistance extends AbstractDb
{
    /**
     * Resource initialization
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Beside\Sourcing\Api\Data\StoreDistanceInterface::STORE_DISTANCE_TABLE, 'id');
    }
}
