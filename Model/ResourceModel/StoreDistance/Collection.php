<?php
declare(strict_types=1);

namespace Beside\Sourcing\Model\ResourceModel\StoreDistance;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 *
 * @package Beside\Sourcing\Model\ResourceModel\StoreDistance
 */
class Collection extends AbstractCollection
{
    /**
     * Entity id field
     *
     * @var string
     */
    protected $_idFieldName = 'id';

    /** @var string */
    protected $_eventPrefix = 'beside_store_distance_collection';

    /** @var string */
    protected $_eventObject = 'beside_store_distance_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Beside\Sourcing\Model\StoreDistance::class,
            \Beside\Sourcing\Model\ResourceModel\StoreDistance::class
        );
    }
}
