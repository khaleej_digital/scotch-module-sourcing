<?php
declare(strict_types=1);

namespace Beside\Sourcing\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface;
use Redbox\Shopfinder\Api\Data\ShopInterface;
use Beside\Sourcing\Api\BaseStoreDistanceInterface;

/**
 * Class StoreDataHelper
 *
 * @package Beside\Sourcing\Model
 */
class StoreDataHelper
{
    /**
     * @var LoggerInterface
     */
    public LoggerInterface $logger;

    /**
     * @var ScopeConfigInterface
     */
    public ScopeConfigInterface $scopeConfig;

    /**
     * StoreDataHelper constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param LoggerInterface $logger
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Check if cross store search is enabled in admin panel
     *
     * @return bool
     */
    public function isCrossStoreSearchEnabled(): bool
    {
        return (bool) $this->scopeConfig->getValue(BaseStoreDistanceInterface::XML_PATH_CROSS_STORES_SEARCH);
    }

    /**
     * Get shop data required for distance calculation
     *
     * @param ShopInterface $shop
     *
     * @return array
     */
    public function getShopData(ShopInterface $shop): array
    {
        $data = [];
        foreach (BaseStoreDistanceInterface::DISTANCE_REQUIRED_FIELDS as $distanceRequiredFiled) {
            $value = $shop->getData($distanceRequiredFiled);
            if (empty($value)) {
                $this->logger->error('Empty required fields for the shop ID ' . $shop->getId());
                $data = [];
                break;
            }
            $data[$distanceRequiredFiled] = $shop->getData($distanceRequiredFiled);
        }

        return $data;
    }
}
