<?php
declare(strict_types=1);

namespace Beside\Sourcing\Model;

use Beside\Sourcing\Api\Data\StoreDistanceInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class StoreDistance
 *
 * @package Beside\Sourcing\Model
 */
class StoreDistance extends AbstractModel implements IdentityInterface, StoreDistanceInterface
{
    /**
     * Init model
     */
    protected function _construct()
    {
        $this->_init(\Beside\Sourcing\Model\ResourceModel\StoreDistance::class);
    }

    /**
     * @return array
     */
    public function getIdentities(): array
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get entity ID
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Get store identifier "from"
     *
     * @return string
     */
    public function getStoreIdFrom(): string
    {
        return $this->getData(self::FROM);
    }

    /**
     * Set store identifier "from"
     *
     * @param string $storeId
     *
     * @return StoreDistanceInterface
     */
    public function setStoreIdFrom(string $storeId): StoreDistanceInterface
    {
        return $this->setData(self::FROM, $storeId);
    }

    /**
     * Get store identifier "to"
     *
     * @return string
     */
    public function getStoreIdTo(): string
    {
        return $this->getData(self::TO);
    }

    /**
     * Set store identifier "to"
     *
     * @param string $storeId
     *
     * @return StoreDistanceInterface
     */
    public function setStoreIdTo(string $storeId): StoreDistanceInterface
    {
        return $this->setData(self::TO, $storeId);
    }

    /**
     * Get distance
     *
     * @return int
     */
    public function getDistance(): int
    {
        return (int) $this->getData(self::DISTANCE);
    }

    /**
     * Set distance
     *
     * @param mixed $distance
     *
     * @return StoreDistanceInterface
     */
    public function setDistance($distance): StoreDistanceInterface
    {
        return $this->setData(self::DISTANCE, $distance);
    }
}
