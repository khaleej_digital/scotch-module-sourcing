<?php
declare(strict_types=1);

namespace Beside\Sourcing\Model;

use Beside\Sourcing\Api\Data\StoreDistanceInterface;
use Beside\Sourcing\Api\Data\StoreDistanceInterfaceFactory;
use Beside\Sourcing\Api\Data\StoreDistanceSearchResultsInterfaceFactory;
use Beside\Sourcing\Api\StoreDistanceRepositoryInterface;
use Beside\Sourcing\Model\ResourceModel\StoreDistance as ResourceStoreDistance;
use Beside\Sourcing\Model\ResourceModel\StoreDistance\CollectionFactory as StoreDistanceCollectionFactory;
use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\Search\SearchResultInterfaceFactory;

/**
 * Class StoreDistanceRepository
 *
 * @package Beside\Sourcing\Model
 */
class StoreDistanceRepository implements StoreDistanceRepositoryInterface
{
    /**
     * @var StoreDistanceFactory
     */
    private StoreDistanceFactory $storeDistanceFactory;

    /**
     * @var ResourceStoreDistance
     */
    private ResourceStoreDistance $resource;

    /**
     * @var StoreDistanceCollectionFactory
     */
    private StoreDistanceCollectionFactory $storeDistanceCollectionFactory;

    /**
     * @var SearchResultInterfaceFactory
     */
    private SearchResultInterfaceFactory $searchResultsFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private CollectionProcessorInterface $collectionProcessor;

    /**
     * StoreDistanceRepository constructor.
     *
     * @param StoreDistanceFactory $storeDistanceFactory
     * @param ResourceStoreDistance $resource
     * @param StoreDistanceCollectionFactory $storeDistanceCollectionFactory
     * @param SearchResultInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        StoreDistanceFactory $storeDistanceFactory,
        ResourceStoreDistance $resource,
        StoreDistanceCollectionFactory $storeDistanceCollectionFactory,
        SearchResultInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->storeDistanceFactory = $storeDistanceFactory;
        $this->resource = $resource;
        $this->storeDistanceCollectionFactory = $storeDistanceCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * Get info about request by request id
     *
     * @param $id
     *
     * @return StoreDistanceInterface
     * @throws NoSuchEntityException
     */
    public function get($id): StoreDistanceInterface
    {
        $storeDistance = $this->storeDistanceFactory->create();
        $this->resource->load($storeDistance, $id);
        if (!$storeDistance->getId()) {
            throw new NoSuchEntityException(__('StoreDistance with ID "%1" does not exist.', $id));
        }

        return $storeDistance;
    }

    /**
     * Get list of request
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var SearchResultInterface $searchResult */
        $searchResult = $this->searchResultsFactory->create();

        $collection = $this->storeDistanceCollectionFactory->create();


        $this->collectionProcessor->process($searchCriteria, $collection);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());

        return $searchResult;
    }

    /**
     * Save request
     *
     * @param StoreDistanceInterface $storeDistance
     *
     * @return StoreDistanceInterface
     * @throws AlreadyExistsException
     */
    public function save(StoreDistanceInterface $storeDistance): StoreDistanceInterface
    {
        $this->resource->save($storeDistance);

        return $storeDistance;
    }

    /**
     * Delete request
     *
     * @param StoreDistanceInterface $storeDistance
     *
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(StoreDistanceInterface $storeDistance): bool
    {
        try {
            $storeDistanceModel = $this->storeDistanceFactory->create();
            $this->resource->load($storeDistanceModel, $storeDistance->getId());
            $this->resource->delete($storeDistanceModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the StoreDistance: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * Delete request by ID
     *
     * @param int|string $id
     *
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id): bool
    {
        return $this->delete($this->get($id));
    }
}
