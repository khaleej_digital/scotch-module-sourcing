<?php
declare(strict_types=1);

namespace Beside\Sourcing\Model;

use Beside\Erp\Api\CreateTransferOrderApiInterface;
use Beside\Sourcing\Api\TransferOrderInterface;
use Beside\Erp\Api\ErpApiInterface;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Magento\Framework\Serialize\Serializer\Json;

/**
 * Class TransferOrder
 *
 * @package Beside\Sourcing\Model
 */
class TransferOrder implements TransferOrderInterface
{
    /**
     * @var ErpApiInterface
     */
    private ErpApiInterface $transferOrderApi;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var Json
     */
    private $json;

    /**
     * TransferOrder constructor.
     *
     * @param CreateTransferOrderApiInterface $transferOrderApi
     * @param Json $json
     * @param LoggerInterface $logger
     */
    public function __construct(
        CreateTransferOrderApiInterface $transferOrderApi,
        LoggerInterface $logger,
        Json $json
    ) {
        $this->transferOrderApi = $transferOrderApi;
        $this->logger = $logger;
        $this->json = $json;
    }

    /**
     * @param array $transObject
     * @param mixed $storeId
     */
    public function addTransferToQueue(array $transObject, $storeId): void
    {
        try {
            $message = $this->json->serialize($transObject);
            $this->transferOrderApi->saveToQueue($message, (int) $storeId);
        } catch (InvalidArgumentException $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * @param string|int $orderId
     * @param array $transfers
     * @param string $mainStore
     * @param string|null $salesChannelId
     * @return array
     */
    public function prepareTransferOrderMessage($orderId, array $transfers, string $mainStore, ?string $salesChannelId = null)
    {
        $transferMessages = [];
        foreach ($transfers as $store => $storeTransfers) {
            foreach ($storeTransfers as $transfer) {
                foreach ($transfer as $sku => $qty) {
                    $transObject = [
                        'order_id' => (string) $orderId,
                        'sku' => (string) $sku,
                        'quantity' => (string) $qty,
                        'source_id' => (string) $store,
                        'destination_id' => (string) $mainStore,
                        'sales_channel_id' => $salesChannelId ? (string) $salesChannelId : null,
                    ];
                    //todo remove redundant logging if needed
                    $this->logger->debug(print_r($transObject, true));

                    $transferMessages[] = $this->json->serialize($transObject);
                }
            }
        }

        return $transferMessages;
    }
}
