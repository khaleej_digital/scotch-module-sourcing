<?php
declare(strict_types=1);

namespace Beside\Sourcing\Observer;

use Beside\Sourcing\Model\StoreDataHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;
use Redbox\Shopfinder\Api\Data\ShopInterface;
use Redbox\Shopfinder\Model\ResourceModel\Shop\Collection as ShopfinderShopCollection;
use Beside\Sourcing\Api\BaseStoreDistanceInterface;

/**
 * Class ShopfinderSaveAfter
 *
 * @package Beside\Sourcing\Observer
 */
class ShopfinderSaveAfter implements ObserverInterface
{
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var ShopfinderShopCollection
     */
    private ShopfinderShopCollection $shopCollection;

    /**
     * @var BaseStoreDistanceInterface
     */
    private BaseStoreDistanceInterface $baseStoreDistance;

    /**
     * @var StoreDataHelper
     */
    private StoreDataHelper $storeDataHelper;

    /**
     * ShopfinderSaveAfter constructor.
     *
     * @param BaseStoreDistanceInterface $baseStoreDistance
     * @param ShopfinderShopCollection $shopCollection
     * @param StoreDataHelper $storeDataHelper
     * @param LoggerInterface $logger
     */
    public function __construct(
        BaseStoreDistanceInterface $baseStoreDistance,
        ShopfinderShopCollection $shopCollection,
        StoreDataHelper $storeDataHelper,
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
        $this->shopCollection = $shopCollection;
        $this->baseStoreDistance = $baseStoreDistance;
        $this->storeDataHelper = $storeDataHelper;
    }

    /**
     * Execute method to store changes in beside_store_distance table
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $shop = $observer->getObject();
        if ($shop->hasFlag(ShopfinderSaveBefore::DISTANCE_CHANGES)) {
            $newShopData = $this->storeDataHelper->getShopData($shop);
            if ($newShopData) {
                $this->recalculateDistances($newShopData);
            }
        }
    }

    /**
     * Call distance recalculation and saving in DB
     *
     * @param array $newShopData
     */
    private function recalculateDistances(array $newShopData): void
    {
        $storeIds = $newShopData[ShopInterface::STORES];
        $collection = $this->shopCollection;
        if (!$this->storeDataHelper->isCrossStoreSearchEnabled() && $storeIds) {
            $collection = $collection->addFieldToFilter('store_id', ['in' => $storeIds]);
        }
        foreach ($collection as $shop) {
            if ($newShopData[ShopInterface::IDENTIFIER] == $shop[ShopInterface::IDENTIFIER]) {
                continue;
            }
            $shop->setData(ShopInterface::STORES, $shop->getData('store_id'));
            $shopData = $this->storeDataHelper->getShopData($shop);
            if ($shopData) {
                try {
                    $this->baseStoreDistance->addDistance($newShopData, $shopData);
                } catch (LocalizedException $e) {
                    $this->logger->error($e->getMessage());
                }
            }
        }
    }
}
