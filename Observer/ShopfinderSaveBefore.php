<?php
declare(strict_types=1);

namespace Beside\Sourcing\Observer;

use Beside\Sourcing\Api\BaseStoreDistanceInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class ShopfinderSaveBefore
 *
 * @package Beside\Sourcing\Observer
 */
class ShopfinderSaveBefore implements ObserverInterface
{
    /**
     * Flag for changed fields required for distance calculation
     */
    public const DISTANCE_CHANGES = 'distance_changes';

    /**
     * Check if required for distance calculation fields has been changed
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $shop = $observer->getObject();
        if ($shop->hasDataChanges()) {
            foreach (BaseStoreDistanceInterface::DISTANCE_REQUIRED_FIELDS as $distanceRequiredFiled) {
                if ($shop->getData($distanceRequiredFiled) !== $shop->getOrigData($distanceRequiredFiled)) {
                    $shop->setFlag(self::DISTANCE_CHANGES, true);
                    break;
                }
            }
        }
    }
}
